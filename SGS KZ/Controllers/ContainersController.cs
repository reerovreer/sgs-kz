using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace SGS_KZ.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ContainersController(SGSContext context) : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Container>>> GetContainers()
    {
        return await context.Containers.ToListAsync();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Container>> GetContainer(Guid id)
    {
        var container = await context.Containers.FindAsync(id);

        if (container == null)
        {
            return NotFound();
        }

        return container;
    }
    [HttpPost]
    public async Task<ActionResult<Container>> AddContainer(Container container)
    {
        context.Containers.Add(container);
        await context.SaveChangesAsync();

        return await GetContainer(container.Id);
    }
    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateContainer(Guid id, Container container)
    {
        if (id != container.Id)
        {
            return BadRequest();
        }
        context.Entry(container).State = EntityState.Modified;
        await context.SaveChangesAsync();
        return NoContent();
    }
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteContainer(Guid id)
    {
        var container = await context.Containers.FindAsync(id);
        if (container == null)
        {
            return NotFound();
        }

        context.Containers.Remove(container);
        await context.SaveChangesAsync();

        return NoContent();
    }
}
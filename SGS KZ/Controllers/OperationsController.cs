using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SGS_KZ.Models;

namespace SGS_KZ.Controllers;

[ApiController]
[Route("api/[controller]")]
public class OperationsController : ControllerBase
{
    private readonly SGSContext _context;

    public OperationsController(SGSContext context)
    {
        _context = context;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Operation>>> GetOperations()
    {
        return await _context.Operations.ToListAsync();
    }

    [HttpGet("by-container/{containerId}")]
    public async Task<ActionResult<IEnumerable<Operation>>> GetOperationsByContainerId(Guid containerId)
    {
        return await _context.Operations.Where(o => o.ContainerId == containerId).ToListAsync();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Operation>> GetOperation(Guid id)
    {
        var operation = await _context.Operations.FindAsync(id);

        if (operation == null)
        {
            return NotFound();
        }

        return operation;
    }

    [HttpPost]
    public async Task<ActionResult<Operation>> AddOperation(Operation operation)
    {
        _context.Operations.Add(operation);
        await _context.SaveChangesAsync();

        return await GetOperation(operation.Id);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateOperation(Guid id, Operation operation)
    {
        if (id != operation.Id)
        {
            return BadRequest();
        }

        _context.Entry(operation).State = EntityState.Modified;

        await _context.SaveChangesAsync();
        
        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteOperation(Guid id)
    {
        var operation = await _context.Operations.FindAsync(id);
        if (operation == null)
        {
            return NotFound();
        }

        _context.Operations.Remove(operation);
        await _context.SaveChangesAsync();

        return NoContent();
    }
}
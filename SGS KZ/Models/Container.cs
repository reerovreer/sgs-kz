using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

public class Container
{
    public Guid Id { get; set; }
    public int Number { get; set; }
    public string Type { get; set; }
    [Column(TypeName = "decimal(10, 2)")]
    public decimal Length { get; set; }
    [Column(TypeName = "decimal(10, 2)")]
    public decimal Width { get; set; }
    [Column(TypeName = "decimal(10, 2)")]
    public decimal Height { get; set; }
    [Column(TypeName = "decimal(10, 2)")]
    public decimal Weight { get; set; }
    public bool IsEmpty { get; set; }
    public DateTime ArrivalDate { get; set; }
}
using Microsoft.EntityFrameworkCore;
using SGS_KZ.Models;

public class SGSContext : DbContext
{
    public SGSContext(DbContextOptions<SGSContext> options)
        : base(options)
    {
    }
    public DbSet<Container> Containers { get; set; }
    public DbSet<Operation> Operations { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        // Настройка моделей
        modelBuilder.Entity<Container>(entity =>
        {
            entity.Property(e => e.Length).HasColumnType("decimal(10, 2)");
            entity.Property(e => e.Width).HasColumnType("decimal(10, 2)");
            entity.Property(e => e.Height).HasColumnType("decimal(10, 2)");
            entity.Property(e => e.Weight).HasColumnType("decimal(10, 2)");
        });

        modelBuilder.Entity<Operation>()
            .HasOne(o => o.Container)
            .WithMany()
            .HasForeignKey(o => o.ContainerId);
    }
}
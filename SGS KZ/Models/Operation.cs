namespace SGS_KZ.Models;

public class Operation
{
    public Guid Id { get; set; }
    public Guid ContainerId { get; set; }
    public Container Container { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public string OperationType { get; set; }
    public string OperatorName { get; set; }
    public string InspectionLocation { get; set; }
}